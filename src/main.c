/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "esp_event_loop.h"
#include "esp_event.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"

#define MAX_APs 20
#define BLINK_GPIO 2


void chip_information()
{
  printf("ESP32 DOIT information:\n");

  esp_chip_info_t chip_info;
  esp_chip_info(&chip_info);

  printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
          chip_info.cores,
          (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
          (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

  printf("silicon revision %d, ", chip_info.revision);

  printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
          (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

  vTaskDelay(1000 / portTICK_PERIOD_MS);
  fflush(stdout);
}

// void blink_task()
void blink_task(void *pvParameter)
{
    /* Configure the IOMUX register for pad BLINK_GPIO (some pads are
       muxed to GPIO on reset already, but some default to other
       functions and need to be switched to GPIO. Consult the
       Technical Reference for a list of pads and their default
       functions.)
    */
    gpio_pad_select_gpio(BLINK_GPIO);

    /* Set the GPIO as a push/pull output */
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);

    // Present the information of the development board
    vTaskDelay(5000 / portTICK_PERIOD_MS);
    chip_information();

    while(1) {
        /* Blink off (output low) */
        gpio_set_level(BLINK_GPIO, 0);
        vTaskDelay(250 / portTICK_PERIOD_MS);
        /* Blink on (output high) */
        gpio_set_level(BLINK_GPIO, 1);
        vTaskDelay(250 / portTICK_PERIOD_MS);
    }
}

// From auth_mode code to string
static char* getAuthModeName(wifi_auth_mode_t auth_mode) {

	char *names[] = {"OPEN", "WEP", "WPA PSK", "WPA2 PSK", "WPA WPA2 PSK", "MAX"};
	return names[auth_mode];
}

// Empty event handler
static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    return ESP_OK;
}

// void wifi_task(void *pvParameter)
void wifi_scan_networks()
{

  /*
  The folliwing link explain the secuence diagram of a wifi connection
  in the Station Mode:
  https://docs.espressif.com/projects/esp-idf/en/latest/api-guides/wifi.html#esp32-wi-fi-station-general-scenario

  Also show and explain the steps to make the connection.
  */

  /*        1. Wi-Fi/LwIP Init Phase       */

  // s1.1: The main task calls tcpip_adapter_init() to create an LwIP core
  // task and initialize LwIP-related work.
  tcpip_adapter_init();

  // s1.2: The main task calls esp_event_loop_init() to create a system
  // Event task and initialize an application event’s callback function.
  // In the scenario above, the application event’s callback function does
  // nothing but relaying the event to the application task.
  ESP_ERROR_CHECK(esp_event_loop_init(&event_handler, NULL));

  // s1.3: The main task calls esp_wifi_init() to create the Wi-Fi driver
  // task and initialize the Wi-Fi driver.
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));

  /*        2. Wi-Fi Configuration Phase       */

  /*
  Once the Wi-Fi driver is initialized, you can start configuring the
  Wi-Fi driver. In this scenario, the mode is Station, so you may need to
  call esp_wifi_set_mode(WIFI_MODE_STA) to configure the Wi-Fi
  mode as Station
  */
  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

  /*        3. Wi-Fi Start Phase       */

  // s3.1: Call esp_wifi_start to start the Wi-Fi driver.
  ESP_ERROR_CHECK(esp_wifi_start());

  /*
  s3.2: The Wi-Fi driver posts <WIFI_EVENT_STA_START> to the event task;
  then, the event task will do some common things and will call the application
  event callback function.
  */

  /*
  s3.3: The application event callback function relays the <WIFI_EVENT_STA_START>
  to the application task. We recommend that you call esp_wifi_connect().
  However, you can also call esp_wifi_connect() in other phrases after
  the <WIFI_EVENT_STA_START> arises.
  */

  // configure and run the scan process in blocking mode
	wifi_scan_config_t scan_config = {
		.ssid = 0,
		.bssid = 0,
		.channel = 0,
    .show_hidden = true
  };

  printf("Start scanning...");
	ESP_ERROR_CHECK(esp_wifi_scan_start(&scan_config, true));
	printf(" completed!\n");
	printf("\n");

  // get the list of APs found in the last scan
	uint16_t ap_num = MAX_APs;
	wifi_ap_record_t ap_records[MAX_APs];
	ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&ap_num, ap_records));

	// print the list
	printf("Found %d access points:\n", ap_num);
	printf("\n");
	printf("               SSID              | Channel | RSSI |   Auth Mode \n");
	printf("----------------------------------------------------------------\n");
	for(int i = 0; i < ap_num; i++)
		printf("%32s | %7d | %4d | %12s\n", (char *)ap_records[i].ssid, ap_records[i].primary, ap_records[i].rssi, getAuthModeName(ap_records[i].authmode));
	printf("----------------------------------------------------------------\n");

}

// Main function to run the code
void app_main()
{
  // initialize NVS
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
     ESP_ERROR_CHECK(nvs_flash_erase());
     ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK( ret );

  // Scan the networks availables
  wifi_scan_networks();

  // Run the blinl task
  xTaskCreate(&blink_task, "blink_task1", 2048, NULL, 5, NULL);
}
